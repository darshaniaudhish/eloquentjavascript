class Vector{
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    plus(vector){
        let newX = this.x + vector.x;
        let newY = this.y + vector.y;
        return new Vector(newX,newY);
    }

    minus(vector){
        return new Vector(this.x-vector.x,this.y-vector.y);
    }

    get length(){
        return Math.sqrt(this.x*this.x+this.y+this.y);
    }
}


let vector1 = new Vector(2,3);
let vector2 = new Vector(1,2);
let plusVector = vector1.plus(vector2);
console.log(plusVector);
let minusVector = vector1.minus(vector2)
console.log(minusVector);
console.log(vector1.length);
