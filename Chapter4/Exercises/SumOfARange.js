function range(start,end,step){
    var rangeArray = [];
    if(start!=null && end!=null){        
        if(step!=null && step!=undefined){
            if(start<=end){
                while(start!=end && start<end){
                    rangeArray.push(start);
                    start+=step;
                }
                if(rangeArray[rangeArray.length-1]+step == end){
                    rangeArray.push(end);
                }                
            }else{
                while(start!=end && start>end ){
                    rangeArray.push(start);
                    start+=step;
                }
                if(rangeArray[rangeArray.length-1]+step == end){
                    rangeArray.push(end);
                }  
            }
        }else{
            step = 1;
            while(start!=end){
                rangeArray.push(start);
                start+=step;
            }
            if(rangeArray[rangeArray.length-1]+step == end){
                rangeArray.push(end);
            }        
        }
    }else console.log("start and end values required");
    return rangeArray;
}

function sum(range){    
    var sum = 0;
    if(range.length !=0){        
        for(var i=0; i<range.length;i++){
            sum +=range[i];
        }        
    }else console.log("range array is empty");
    return sum;
}
console.log(sum(range(1,10)));
console.log(sum(range(1,10,2)));
console.log(sum(range(5,2,-1)));