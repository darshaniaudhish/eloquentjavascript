## Key points

- Higher Order Functions: Functions that operate on other functions, either by taking them as arguments or by returning them, are called higher-order functions.
- Closures


## Functional programming in JS:
- use Higher order functions
- use closures
- Don't use iterations. Instead use array functions: filter, map, reduce.
- Don't use iterations. Instead use recursion.
- Pure functions: no side effects
- Immutability: Persistent data structures
- Flow data through functions{output of first function becomes input to next function and its output becomes input to next function}


## References:
- [Introduction to functional programming by Mary Rose Cook](https://maryrosecook.com/blog/post/a-practical-introduction-to-functional-programming)
- [Ideal Hash Trees by Phil Bagwell](https://lampwww.epfl.ch/papers/idealhashtrees.pdf)
