function ArrayToList(array){
    var list={};

    for(var i=array.length-1;i>0;i--){
        var tempList = {};             
        tempList.value = array[i];
        if(i==array.length-1){ 
            tempList.rest = null;           
            list.rest = tempList;   
        }
        else{
            var restObj = list.rest;
            tempList.rest = restObj;
            list.rest = tempList;
        }               
    }
    list.value = array[i];
    return list;  
}

function ListToArray(list){
    var array=[];
    while(list.rest!=null){
        array.push(list.value);
        list=list.rest;
    }
    array.push(list.value);
    return array;
}
 
function prepend(element,list){
    var tempList={};
    tempList.value = element;
    tempList.rest = list;
    return tempList;
}

function nth(list,number){
    for(var i=0;i<=number;i++){
        var element = list.value;
        list = list.rest;
    }
    return element;
}

var array1 = [12,34,65,76,87];
var list = ArrayToList(array1);
console.log(list);
var array = ListToArray(list);
console.log(array);
var prependedList = prepend(25,list);
console.log(prependedList);
console.log(nth(list,3));