class Group{
    constructor(){
        this.members = [];
    }

    add(value){
        if(!this.members.includes(value))
            this.members.push(value);   
    }

    delete(value){
        if(this.members.includes(value))
            this.members = this.members.filter((x) => x!==value);
    }

    has(value){
      return this.members.includes(value);
    }

    static from(object){
        let group = new Group();
        object.map((x) => {
            if(!(x in group.members))
            {
                group.members.push(x)
            }
        });
        return group;
    }
}


let group = Group.from([10, 20]);
console.log(group.has(10));
// → true
console.log(group.has(30));
// → false
group.add(10);
group.delete(10);
console.log(group.has(10));
// → false