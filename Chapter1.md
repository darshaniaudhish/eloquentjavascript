## Key points

- ECMAScript/Javascript
- Special numbers(Infinity, -Infinity, NaN)
- Undefined values(Undefined and null)
- Type Coercion
- Short circuiting of logical operators (|| and &&)
