var array = [[1,2,3],[2,3,4],[3,4,5]];
console.log(array.reduce((initial,current) => initial.concat(current),[] ));

//Another way to write the same thing
function flat(initial,current){
    return initial.concat(current);
}
console.log(array.reduce(flat,[]));