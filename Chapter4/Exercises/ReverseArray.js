function reverseArray(array){
    var reversedArray = [];
    for(var i=array.length;i>=0;i--){
        reversedArray.push(array[i]);
    }
    return reversedArray;
}

function reverseArrayInPlace(array){
    var arrayMid = Math.floor(array.length/2);
    for(var i=0;i<arrayMid;i++){
        var temp =array[i];
        array[i]= array[array.length-1-i];
        array[array.length-1-i]=temp;
    }
    console.log(array);
}

var array1 = [1,3,6,8,12];
var array2 = [14,34,64,84,124,224];
console.log(reverseArray(array1));
console.log(reverseArray(array2));
reverseArrayInPlace(array1);
reverseArrayInPlace(array2);